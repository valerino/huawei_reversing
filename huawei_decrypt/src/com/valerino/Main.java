package com.valerino;

import java.io.*;

public class Main {
    /**
     * all the following has been ripped from com.huawei.iaware package (HwIAware.apk) in the android.rms.iaware.IAwareDecrypt class
     */
    private static final byte[] COMPONENT = {-118, 80, -33, -103, 100, 101, 125, -35, -28, -46, -66, -15, -36, 5, -79, 115};
    private static final byte[] COMPONENT2 = {-91, 55, 87, -46, 64, -10, 24, 58, 50, Byte.MIN_VALUE, -42, -77, -62, 118, 112, 29, -60, 16, -94, -35, 17, 46, 68, -80, 40, -58, -25, -90, -5, 36, -84, 27};
    private static final byte[] COMPONENT3 = {2, 13, 17, 7, -66, -97, 55, -95, 85, -25, 74, 56, 96, 112, -122, 66};
    private static final java.lang.String COMPONENT_NAME_1 = "iaware_c.dat";
    private static final java.lang.String COMPONENT_NAME_2 = "iaware_cm.dat";
    private static final java.lang.String ENCYPTION_SCHEME = "AES";
    private static final byte[] UTF8_BOM_HEAD = {-17, -69, -65};
    private static final java.lang.String XML_HEAD = "<?xml";
    private static String cmPath = "";
    public static java.io.InputStream decryptInputStream(java.io.InputStream ins) {
        java.io.InputStream inputStream;
        if (ins == null) {
            return ins;
        }

        long start = java.lang.System.currentTimeMillis();
        if (!ins.markSupported()) {
            inputStream = getByteArrayInputStream(ins);
        } else {
            inputStream = ins;
        }
        if (inputStream == null) {
            return null;
        }
        if (isNormalXml(inputStream)) {
            return inputStream;
        }
        java.io.InputStream ret = null;
        javax.crypto.CipherOutputStream cipherOutputStream = null;
        try {
            if (!isStreamAvailable(inputStream)) {
                closeStream(null);
                closeStream(inputStream);
                return null;
            }
            javax.crypto.Cipher cipher = getCipher(inputStream);
            if (cipher == null) {
                closeStream(null);
                closeStream(inputStream);
                return null;
            } else if (inputStream.skip(16) != 16) {
                closeStream(null);
                closeStream(inputStream);
                return null;
            } else {
                java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
                javax.crypto.CipherOutputStream cipherOutputStream2 = new javax.crypto.CipherOutputStream(baos, cipher);
                byte[] buffer = new byte[1024];
                while (true) {
                    int r = inputStream.read(buffer);
                    if (r == -1) {
                        break;
                    }
                    cipherOutputStream2.write(buffer, 0, r);
                }
                closeStream(cipherOutputStream2);
                cipherOutputStream = null;
                ret = new java.io.ByteArrayInputStream(baos.toByteArray());
                closeStream(cipherOutputStream);
                closeStream(inputStream);
                System.out.println("decryptInputStream decrypt spend " + (java.lang.System.currentTimeMillis() - start) + "ms!");
                return ret;
            }
        } catch (java.io.IOException e) {
            System.out.println("decryptFile IOException!");
        } catch (java.lang.Throwable th) {
            closeStream(null);
            closeStream(inputStream);
            throw th;
        }
        finally {
            closeStream(ret);
            closeStream(inputStream);
        }
        return null;
    }

    /* JADX INFO: finally extract failed */
    private static java.io.ByteArrayInputStream getByteArrayInputStream(java.io.InputStream inputStream) {
        java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                int r = inputStream.read(buffer);
                if (r != -1) {
                    bos.write(buffer, 0, r);
                } else {
                    java.io.ByteArrayInputStream byteArrayInputStream = new java.io.ByteArrayInputStream(bos.toByteArray());
                    closeStream(inputStream);
                    return byteArrayInputStream;
                }
            } catch (java.io.IOException e) {
                System.out.println("getByteArrayInputStream IOException!");
                closeStream(inputStream);
                return null;
            } catch (java.lang.Throwable th) {
                closeStream(inputStream);
                throw th;
            }
        }
    }

    private static boolean isNormalXml(java.io.InputStream inputStream) {
        byte[] head = new byte[5];
        try {
            byte[] bomHead = new byte[3];
            if (inputStream.read(bomHead) != 3 || !java.util.Arrays.equals(bomHead, UTF8_BOM_HEAD)) {
                inputStream.reset();
            }
            int r = inputStream.read(head);
            inputStream.reset();
            if (r != 5 || !XML_HEAD.equals(new java.lang.String(head, "utf-8"))) {
                return false;
            }
            return true;
        } catch (java.io.IOException e) {
            System.out.println("isNormalXml IOException!");
            return true;
        }
    }

    private static javax.crypto.Cipher initAESCipher(byte[] codeFormate, byte[] iv) {
        if (codeFormate == null || iv == null) {
            return null;
        }
        try {
            javax.crypto.spec.SecretKeySpec key = new javax.crypto.spec.SecretKeySpec(codeFormate, ENCYPTION_SCHEME);
            javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(2, key, new javax.crypto.spec.IvParameterSpec(iv));
            return cipher;
        } catch (java.security.NoSuchAlgorithmException e) {
            System.out.println("initAESCipher NoSuchAlgorithmException!");
            return null;
        } catch (javax.crypto.NoSuchPaddingException e2) {
            System.out.println("initAESCipher NoSuchPaddingException!");
            return null;
        } catch (java.security.InvalidKeyException e3) {
            System.out.println("initAESCipher InvalidKeyException!");
            return null;
        } catch (java.security.InvalidAlgorithmParameterException e4) {
            System.out.println("initAESCipher InvalidAlgorithmParameterException!");
            return null;
        } catch (java.lang.IllegalArgumentException e5) {
            System.out.println("initAESCipher IllegalArgumentException!");
            return null;
        }
    }

    private static javax.crypto.Cipher getCipher(java.io.InputStream inputStream) throws java.io.IOException {
        return initAESCipher(parseCodeFormate(), parseComponent(inputStream, 0, 16));
    }

    private static void closeStream(java.io.Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (java.io.IOException e) {
                System.out.println("close inputStream error!");
            }
        }
    }

    private static byte[] parseCodeFormate() {
        javax.crypto.Cipher cipher = initAESCipher(getRootComponent(), COMPONENT3);
        if (cipher == null) {
            return new byte[0];
        }
        try {
            return cipher.doFinal(COMPONENT2);
        } catch (javax.crypto.IllegalBlockSizeException e) {
            System.out.println("parseCodeFormate IllegalBlockSizeException!");
            return new byte[0];
        } catch (javax.crypto.BadPaddingException e2) {
            System.out.println("parseCodeFormate BadPaddingException!");
            return new byte[0];
        }
    }

    private static byte[] getRootComponent() {
        return generateKey(getComponentFromAssets(COMPONENT_NAME_1), getComponentFromAssets(COMPONENT_NAME_2));
    }

    private static boolean isStreamAvailable(java.io.InputStream inputStream) throws java.io.IOException {
        return inputStream.available() > 16;
    }

    private static byte[] parseComponent(java.io.InputStream inputStream, int start, int len) throws java.io.IOException {
        byte[] zeroByte = new byte[0];
        inputStream.reset();
        byte[] buffer = new byte[len];
        if (inputStream.skip((long) start) != ((long) start)) {
            inputStream.reset();
            return buffer;
        }
        if (inputStream.read(buffer, 0, len) != len) {
            buffer = zeroByte;
        }
        inputStream.reset();
        return buffer;
    }

    private static byte[] generateKey(byte[] c1, byte[] c2) {
        return cutByteArray(hashCompoent(XORBytes(gression(XORBytes(cutByteArray(hashCompoent(gression(COMPONENT, 16, true, 4))), c1, 16), 16, false, 1), c2, 16)));
    }

    private static byte[] gression(byte[] component, int len, boolean leftShift, int bit) {
        if (component == null || component.length != len) {
            return new byte[0];
        }
        byte[] ret = new byte[len];
        for (int i = 0; i < len; i++) {
            if (leftShift) {
                ret[i] = (byte) (component[i] << bit);
            } else {
                ret[i] = (byte) (component[i] >> bit);
            }
        }
        return ret;
    }

    private static byte[] XORBytes(byte[] c1, byte[] c2, int len) {
        byte[] zeroByte = new byte[0];
        if (c1 == null || c1.length != len || c2 == null || c2.length != len) {
            return zeroByte;
        }
        byte[] ret = new byte[len];
        for (int i = 0; i < len; i++) {
            ret[i] = (byte) (c1[i] ^ c2[i]);
        }
        return ret;
    }

    private static byte[] hashCompoent(byte[] component) {
        byte[] zeroByte = new byte[0];
        if (component == null || component.length <= 0) {
            return zeroByte;
        }
        try {
            java.security.MessageDigest digest = java.security.MessageDigest.getInstance("SHA-256");
            digest.update(component);
            return digest.digest();
        } catch (java.security.NoSuchAlgorithmException e) {
            System.out.println("No SHA-256 algorithm found!");
            return zeroByte;
        }
    }

    private static byte[] cutByteArray(byte[] res) {
        byte[] zeroByte = new byte[0];
        if (res == null || res.length < 16) {
            return zeroByte;
        }
        byte[] dst = new byte[16];
        java.lang.System.arraycopy(res, 0, dst, 0, 16);
        if (dst.length != 16) {
            return null;
        }
        return dst;
    }

    private static byte[] getComponentFromAssets(java.lang.String name) {
        byte[] zeroByte = new byte[0];
        java.io.InputStream in = null;
        try {
            // component files are relative to the given cmPath
            File f = new File(new File(cmPath),name);
            in = new FileInputStream(f);
            if (in == null) {
                closeStream(in);
                return zeroByte;
            }
            byte[] buffer = new byte[16];
            if (in.read(buffer) == 16) {
                closeStream(in);
                return buffer;
            }
            closeStream(in);
            return zeroByte;
        } catch (java.io.IOException e) {
            System.out.println("Assets Exception!");
        } catch (java.lang.Throwable th) {
            closeStream(null);
            throw th;
        }
        finally {
            closeStream(in);
        }
        return null;
    }

    static void inputToOutputStream(InputStream src, OutputStream dst) throws IOException {
        while (true) {
            byte[] buf = new byte[1024];
            int len = src.read(buf);
            if (len < 0) {
                break;
            }
            dst.write(buf, 0, len);
        }
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("usage: hw_decrypt.jar <path/to/keymaterial> <path/to/encrypted_xml> <path/to/decryptedxml>");
            System.out.println("\tkeymaterial is a folder containing iaware_c.dat and iaware_cm.dat extracted from Huawei's com.huawei.iaware apk (usually /system/app/HwIAware/HwIAware.apk).");
            return;
        }

        // validate input
        File cm = new File(args[0]);
        File inXml = new File(args[1]);
        File outXml = new File(args[2]);
        System.out.println(String.format("[.] looking for iaware_c.dat and iaware_cm.dat in %s",cm));
        System.out.println(String.format("[.] input encrypted xml=%s",inXml));
        System.out.println(String.format("[.] output decrypted xml=%s",outXml));
        FileInputStream ffin = null;
        try {
            ffin = new FileInputStream(inXml);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        // decrypt
        cmPath = cm.getAbsolutePath();
        InputStream dec = decryptInputStream(ffin);
        FileOutputStream decOut = null;
        try {
            outXml.delete();
            decOut = new FileOutputStream(outXml.getAbsolutePath());
            inputToOutputStream(dec,decOut);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        System.out.println("[.] done!");
    }
}
