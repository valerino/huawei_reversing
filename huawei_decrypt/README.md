# huawei_decrypt

tool to decrypt PowerGenie, IAware and other encrypted XMLs found in recent (EMUI9+ ? ) huawei devices.

__needs iaware_c.dat and iaware_cm.dat extracted from the IAWare APK package on a device__

~~~bash

# usage
java -jar ./out/artifacts/hw_decrypt_jar/hw_decrypt.jar . ./iaware_config_cust.xml ./decrypted.xml
~~~