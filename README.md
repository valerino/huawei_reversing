# huawei reversing stuff

random stuff about [reversing](./dex) [huawei](./framework) apps, especially about power management apps which keeps my evil apps from persisting in background :)

using [./huawei_decrypt](huawei_decrypt/README.md) you can also [decrypt encrypted XMLs](./xml) found on newer (EMUI 9+) huawei devices.

also, [dontkillymyapp.com official app](./com.urbandroid.dontkillmyapp_27_apps.evozi.com.apk) is provided for testing [after editing your apk](https://bitbucket.org/valerino/apk_utils/src/master/]) :)

> __apps and framework extracted from a P40 Lite with EMUI 10__

## interesting package names

> look in decompiled dex for hardcoded packages, or in [xml extracted from /system/emui](./xml)

* com.lunarlabs.panda (seems a sort of internal test apps, __completely disables all powermanagement__, 100% score in dontkillmyapp app opposite to 3% using default setting)
* com.huawei.magnifier (hides app in the dashboard)
* ...

have fun :)
